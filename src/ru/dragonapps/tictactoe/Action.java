package ru.dragonapps.tictactoe;

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.Random;

import static ru.dragonapps.tictactoe.GamePanel.cells;

public class Action extends AbstractAction {

    int x, y, xUI, yUI;
    JButton btn;
    boolean xodUi;

    @Override
    public void actionPerformed(ActionEvent e) {
        btn = (JButton) e.getSource();

        if (btn.getName() == null) {

            if (xodUi == false) {
                if (btn.getX() == 0) x = 0;
                if (btn.getX() == 166) x = 1;
                if (btn.getX() == 332) x = 2;

                if (btn.getY() == 0) y = 0;
                if (btn.getY() == 166) y = 1;
                if (btn.getY() == 332) y = 2;

                cells[y][x].setText("X");
                cells[y][x].setName("X");
                xodUi = true;
                ui();
                testWin();
            }
        }
    }

    public int ui() {

        for (int x = 0; x <= 2; x++) {

            for (int y = 0; y <= 2; y++) {

                yUI = new Random().nextInt(3);
                xUI = new Random().nextInt(3);
                if (cells[yUI][xUI].getName() == null && xodUi == true) {
                    cells[yUI][xUI].setText("0");
                    cells[yUI][xUI].setName("0");
                    xodUi = false;
                }
            }
        }

        return 1;
    }

    public boolean testWin() {
        boolean testWin = false;

        for (int y = 0; y <= 2; y++) {

            if (cells[y][0].getName() == "X"
                    && cells[y][1].getName() == "X"
                    && cells[y][2].getName() == "X"
                    && testWin != true) {

                testWin = true;
                JOptionPane.showMessageDialog(null, "Your Win!!!");
                clear();


            } else
                testWin = false;
        }

        for (int x = 0; x <= 2; x++) {
            if (cells[0][x].getName() == "X"
                    && cells[1][x].getName() == "X"
                    && cells[2][x].getName() == "X"
                    && testWin != true) {

                testWin = true;
                JOptionPane.showMessageDialog(null, "Your Win!!!");
                clear();

            } else
                testWin = false;
        }

        return testWin;
    }

    public int clear() {

        for (int x = 0; x <= 2; x++) {

            for (int y = 0; y <= 2; y++) {
                cells[y][x].setText(null);
                cells[y][x].setName(null);
                xodUi = false;
            }
        }

        return 1;
    }
}


