package ru.dragonapps.tictactoe;


import javax.swing.*;

public class Frame extends JFrame {

    public Frame(int width, int height, String name, JPanel mainPanel) {
        setTitle(name);
        setSize(width,height);
        setLocationRelativeTo(null);
        add(mainPanel);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        setVisible(true);
    }

}
