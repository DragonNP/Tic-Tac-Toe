package ru.dragonapps.tictactoe;

import javax.swing.*;

public class MainPanel extends JPanel {

    GamePanel gamePanel;

    public MainPanel(){
        setLayout(null);

        gamePanel = new GamePanel();
        gamePanel.setSize(500,500);
        gamePanel.setLocation(150,0);
        add(gamePanel);
    }
}
