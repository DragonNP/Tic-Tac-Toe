package ru.dragonapps.tictactoe;

import javax.swing.*;
import javax.swing.Action;
import java.awt.*;

public class GamePanel extends JPanel {

    public static JButton[][] cells;
    int cellsY;
    Font FontButtons;
    Action action;

    public GamePanel() {
        setLayout(null);

        cells = new JButton[3][3];
        FontButtons = new Font("TimesRoman", Font.BOLD,   166);
        action = new ru.dragonapps.tictactoe.Action();

        for (byte y = 0; y < 3; y++) {

            cells[y][0] = new JButton();
            cells[y][0].setSize(166, 166);
            cells[y][0].setLocation(0, cellsY);
            cells[y][0].setAction(action);
            cells[y][0].setFont(FontButtons);
            add(cells[y][0]);

            for (byte x = 1; x < 3; x++) {
                cells[y][x] = new JButton();
                cells[y][x].setSize(166, 166);
                cells[y][x].setLocation(cells[y][--x].getX() + 166, cellsY);
                x++;
                cells[y][x].setAction(action);
                cells[y][x].setFont(FontButtons);
                add(cells[y][x]);
            }
            cellsY += 166;
        }
    }
}
