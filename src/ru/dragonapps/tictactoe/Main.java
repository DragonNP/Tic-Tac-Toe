package ru.dragonapps.tictactoe;

public class Main {

    Frame frame;
    int width, height;
    String name;
    MainPanel mainPanel;

    public Main() {
        width = 150*2+500;
        height = 525;
        name = "Крестики Нолики";
        mainPanel = new MainPanel();
        frame = new Frame(width, height, name, mainPanel);
    }

    public static void main(String[] args) {
        new Main();
    }
}
